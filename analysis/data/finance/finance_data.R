library(readxl)
library(readr)

#Set specific working directory
setwd("C:/Users/Kadri Gbemi/Desktop/thesis/segregationdataanalysis/analysis/data/")

#Download all finance data
cash_raw_data <- read_excel("finance/cash_spent/cash_data.xlsx", skip = 1)
current_assets_raw_data <- read_excel("finance/current-assets/current-assets-data.xlsx")
current_liabilities_raw_data <- read_excel("finance/current-liabilities/current_liabilities_data.xlsx")
income_raw_data <- read_excel("finance/income/income_data.xlsx")
inventories_raw_data <- read_excel("finance/inventories/inventories_data.xlsx")
prepayments_raw_data <- read_excel("finance/prepayments/prepayments_data.xlsx")
supplier_payable_raw_data <- read_excel("finance/supplier-payable/supplier_payable_data.xlsx")
total_liabilities_raw_data <- read_excel("finance/total-liabilities/total_liabilities_data.xlsx")
total_assets_raw_data <- read_excel("finance/total-assets/total_assets_data.xlsx", skip = 5)

#Download segregation data
scube_data_raw <- read.csv(file="segregation/segregation_data.csv",
                           header=TRUE, sep=",", fileEncoding = "UTF-8")

translate_county_value <- function(data){
  data$County <- sub(" county", "", data$County)
  return(data)
}

cash_data <- translate_county_value(cash_raw_data)
current_assets_data <- translate_county_value(current_assets_raw_data)
current_liabilities_data <- translate_county_value(current_liabilities_raw_data)
income_data <- translate_county_value(income_raw_data)
inventories_data <- translate_county_value(inventories_raw_data)
prepayments_data <- translate_county_value(prepayments_raw_data)
supplier_payable_data <- translate_county_value(supplier_payable_raw_data)
total_liabilities_data <- translate_county_value(total_liabilities_raw_data)
total_assets_data <- translate_county_value(total_assets_raw_data)

finance_data <- Reduce(function(x,y) merge(x = x, y = y, by=c("County", "timeUnit")),
       list(cash_data, current_assets_data, current_liabilities_data, total_liabilities_data,
            income_data, total_assets_data, inventories_data, prepayments_data, supplier_payable_data))

finance_data$CurrentRatio <- finance_data$CurrentAssetsYearEnd/ finance_data$CurrentLiabilitiesYearEnd 
finance_data$QuickRatio <- (finance_data$CurrentAssetsYearEnd - finance_data$InventoriesYearEnd - finance_data$prepaymentYearEnd)/finance_data$CurrentLiabilitiesYearEnd 
finance_data$BurnRate <- (finance_data$CashYearBegin - finance_data$CashYearEnd)/12 
finance_data$Runway <- finance_data$CashYearEnd / finance_data$BurnRate
finance_data$SupplierPayable <- finance_data$supplierPayablesYearBegin
finance_data$AvgShareHolderEquity <- finance_data$totalAssetsYearEnd/ finance_data$LiabilitiesTotalYearEnd 
finance_data$ReturnOnEquity <- (finance_data$FinancialIncome / finance_data$AvgShareHolderEquity)*100

finance_scube_data <- merge(finance_data, scube_data_raw, by=c("County", "timeUnit"))

finance_data_main <- select(finance_data, c("County", "timeUnit", "CurrentRatio", 
                                            "QuickRatio", "BurnRate", "Runway",
                                            "ReturnOnEquity"))

## Export merged Scube data with labor data
write_csv(finance_scube_data, "finance/finance_data.csv")

#For well-being data
write_csv(finance_data_main, "finance/finance_data_well_being.csv")
